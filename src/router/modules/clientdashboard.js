/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const clientDashboardRouter = {
  path: '/clientdashboard',
  component: Layout,
  children: [
    {
      path: 'index',
      component: () => import('@/views/clientdashboard/index'),
      name: 'clientDashboard',
      meta: { title: '数据报表', icon: 'dashboard', noCache: true }
    }
  ]
};
export default clientDashboardRouter
