/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const clientRouter = {
  path: '/clientcommon',
  component: Layout,
  children: [
    {
      path: 'index',
      component: () => import('@/views/clientcommon/index'),
      name: 'clientCommonManage',
      meta: { title: '公海管理', icon: 'el-icon-suitcase', noCache: true }
    }
  ]
};
export default clientRouter
