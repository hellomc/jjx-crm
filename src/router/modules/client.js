/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

// const clientRouter = {
//   path: '/client',
//   component: Layout,
//   children: [
//     {
//       path: 'index',
//       component: () => import('@/views/client/index'),
//       name: 'clientManage',
//       meta: {title: '客户管理', icon: 'user', noCache: true}
//     },
//     {
//       path: 'follow/:cid/:type',
//       component: () => import('@/views/client/follow'),
//       name: 'clientFollow',
//       hidden: true,
//       meta: {title: '追踪记录', icon: 'user', noCache: true}
//     },
//   ]
// };

const clientRouter = {
  path: '/client',
  component: Layout,
  redirect: '/client/index',
  children: [
    {
      path: 'index',
      component: () => import('@/views/client/index'),
      name: 'clientManage',
      meta: {title: '客户管理', icon: 'user', noCache: true},
    },
    {
      path: 'follow',
      component: () => import('@/views/client/follow'),
      name: 'clientFollow',
      hidden: true,
      meta: {title: '追踪记录', icon: 'user', noCache: true}
    },
  ]
};
export default clientRouter
