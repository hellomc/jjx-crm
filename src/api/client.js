import request from '@/utils/request'

// 创建客户
export function createClient(query) {
  return request({
    url: '/crmsystem/client/createClient',
    method: 'post',
    params: query
  })
}

// 查询客户
export function getClientInfo(query) {
  return request({
    url: '/crmsystem/client/clientInfo',
    method: 'get',
    params: query
  })
}

// 更新客户
export function updateClient(query) {
  return request({
    url: '/crmsystem/client/updateclient',
    method: 'post',
    params: query
  })
}

// 客户投入公海
export function dropClient(query) {
  return request({
    url: '/crmsystem/client/dropClient',
    method: 'post',
    params: query
  })
}

// 客户从公海被捞起
export function pullClient(query) {
  return request({
    url: '/crmsystem/client/pullclient',
    method: 'post',
    params: query
  })
}

// 删除客户
export function delClient(query) {
  return request({
    url: '/crmsystem/client/delClient',
    method: 'post',
    params: query
  })
}

// 查客户重复
export function clientRepeat(query) {
  return request({
    url: '/crmsystem/client/checkRepeat',
    method: 'get',
    params: query
  })
}

// 客户列表
export function clientList(query) {
  return request({
    url: '/crmsystem/client/clientList',
    method: 'get',
    params: query
  })
}

// 客户公海列表
export function clientCommonList(query) {
  return request({
    url: '/crmsystem/client/clientCommonList',
    method: 'get',
    params: query
  })
}

// 客户行业列表
export function clientIndustryList(query) {
  return request({
    url: '/crmsystem/client/clientIndustryList',
    method: 'get',
    params: query
  })
}

// 客户来源列表
export function clientSourceList(query) {
  return request({
    url: '/crmsystem/client/clientSourceList',
    method: 'get',
    params: query
  })
}

// 客户追踪记录列表
export function clientRecordList(query) {
  return request({
    url: '/crmsystem/client/clientRecordList',
    method: 'get',
    params: query
  })
}

// 客户追踪记录创建
export function clientRecordCreate(query) {
  return request({
    url: '/crmsystem/client/createClientRecord',
    method: 'post',
    params: query
  })
}

// 客户追踪记录删除
export function clientRecordDel(query) {
  return request({
    url: '/crmsystem/client/delClientRecord',
    method: 'post',
    params: query
  })
}

// 客户追踪记录修改
export function clientRecordUpdate(query) {
  return request({
    url: '/crmsystem/client/updateClientRecord',
    method: 'post',
    params: query
  })
}
