import request from '@/utils/request'

export function upload(data) {
  return request({
    url: '/crmsystem/login/upload',
    method: 'post',
    data
  })
}

export function login(data) {
  return request({
    url: '/crmsystem/login/login',
    method: 'post',
    data
  })
}

export function updatePassword(data) {
  return request({
    url: '/crmsystem/login/updatePw',
    method: 'post',
    data
  })
}

// export function getInfo(token) {
//   return request({
//     url: '/vue-element-admin/user/info',
//     method: 'get',
//     params: { token }
//   })
// }
export function getInfo(token) {
  return request({
    url: '/crmsystem/admin/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}
